from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from server import views


urlpatterns = [
    url(r'^', include('feed_crawler.urls', namespace='feed_crawler')),
    url(r'^accounts/login/$', auth_views.LoginView.as_view(
        template_name='login.html', redirect_authenticated_user=True),
        name='login'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
