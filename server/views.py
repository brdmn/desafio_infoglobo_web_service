# -*- coding: utf-8 -*-

#
# Copyright (C) 2013-2017,
# TGR - Tecnologia em Gestão de Redes S.A.
#

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import redirect


@login_required()
def logout_user(request):
    logout(request)
    return HttpResponseRedirect('../')
