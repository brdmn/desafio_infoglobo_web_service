#!/usr/bin/python
# -*- coding: utf-8 -*-

from core.lib import XMLTool
from unittest import TestCase, main


class XML_Test(TestCase):

    def setUp(self):
        pass

    def test_xml_to_json(self):

        xml_obj = XMLTool(xml_file='./tests/small_test.xml')

        json_expected = {"feed": [{"description":
                                   [{"content": "http://img_teste.com",
                                     "type": "image"},
                                    {"content": "Paragraph test",
                                     "type": "text"},
                                    {"content":
                                     ["link1.com", "link2.com", "link3.com"],
                                     "type": "links"}],
                                   "title": "Small Test"}]}

        json_output = xml_obj.xml_to_json()

        self.assertEqual(json_expected, json_output)


if __name__ == '__main__':
    main()
