# Desafio Infoglobo

This is a feed crawler for : http://revistaautoesporte.globo.com/rss/ultimas/feed.xml
You can convert this XML feed to a JSON file like: 

This version use a Django Web Service

```xml
    {
        'feed': [
            {
                'title': 'titulo da materia',
                'link': 'url da materia',
                'description': [
                    {
                        'type': 'text',
                        'content': 'conteudo da tag'
                    },
                    {
                        'type': 'image',
                        'content': 'url da imagem'
                    },
                    {
                        'type': 'links',
                        'content': ['urls dos links', ...]
                    }
                ]
            }
        ]
    }
```
### Prerequisites :

    1. Unix Shell
    2. Python >= 2.7 
    3. BeautifulSoup package
    4. Django 1.11
    
    To install packages you can run:
    1. pip install -r requirements.txt


### Running:

    You can run this code doing:
        1. cd desafio_infoglobo_web_service
        2. python manage.py runserver
        3. Go to http://localhost:8000
        4. Login : infoglobo Password : info@123 

### Tests:
    
    You can run tests doing:
        1. cd desafio_infoglobo_web_service
        2. python -m unittest discover