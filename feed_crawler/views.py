# -*- coding: utf-8 -*-

import json
import os

from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings

from wsgiref.util import FileWrapper
from core.lib import XMLTool

base_dir = settings.BASE_DIR


@csrf_exempt
@login_required()
def index(request):

    if request.method == 'GET':
        return render(request, 'base.html')

    if request.method == 'POST':

        # Defining XML source
        URL = "http://revistaautoesporte.globo.com/rss/ultimas/feed.xml"

        # Creating XMLTool object (passing the URL)
        xml_obj = XMLTool(xml_url=URL)

        # Converting XML to JSON and saving in a .json file
        json_obj = xml_obj.xml_to_json()

        json_obj = json.dumps(json_obj, sort_keys=True,
                              indent=3, separators=(',', ': '),
                              ensure_ascii=False).encode('utf8')

        with open(base_dir + '/feed_crawler/tmp/feed.json', "w+") as json_file:
            json_file.write(json_obj)

        return send_file(base_dir + '/feed_crawler/tmp/feed.json', "feed.json")


def send_file(filename, download_name):

    wrapper = FileWrapper(open(filename))
    response = HttpResponse(wrapper, content_type='application/force-download')
    response['Content-Length'] = os.path.getsize(filename)
    response['Content-Disposition'] = "attachment; filename=%s" % download_name

    return response
